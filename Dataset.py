#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Pandas, OS) y extraer funcionalidades
# a utilizarse en el desarrollo del programa.

import os
import pandas as pd
import matplotlib.pyplot as plt

ciclo_menu = 0


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: función "abrir archivo" y "leer archivo"
def op_file():
    # Se "limpia" la pantalla
    os.system("clear")
    # Para cerciorarse de la existencia del archivo .csv a manipular, se
    # incluyen excepciones
    try:
        # food es el alias al que se le otorga el valor
        food = pd.read_csv("./suministro_alimentos_kcal.csv",
                           index_col="Country")
    except FileNotFoundError:
        os.system("clear")
        print("""
        \n\tEl archivo al cual se hace referencia no existe/no está en el
        directorio actual, favor intente nuevamente.
        """)

    return food


#   2do Módulo: Eliminación de datos innecesarios para la evaluación de datos
#               requeridos
def delete_xy(food):
    os.system("clear")
    food.drop(['Animal fats',
               'Aquatic Products, Other',
               'Cereals - Excluding Beer',
               'Fish, Seafood',
               'Fruits - Excluding Wine',
               'Milk - Excluding Butter',
               'Miscellaneous',
               'Offals',
               'Oilcrops',
               'Pulses',
               'Spices',
               'Starchy Roots',
               'Stimulants',
               'Sugar Crops',
               'Sugar & Sweeteners',
               'Treenuts',
               'Vegetal Products',
               'Vegetable Oils',
               'Vegetables',
               'Obesity',
               'Undernourished',
               'Deaths',
               'Recovered',
               'Active',
               'Unit (all except Population)'
               ],
              axis=1,
              inplace=True)
    return food


#   3er Módulo: Respuesta a la pregunta n° 1:
#   > El sig. módulo tiene por función retornar todos los valores asociados a
#       Chile.
def conf_cl(food):
    os.system("clear")
    food = pd.read_csv("./suministro_alimentos_kcal.csv",
                       index_col="Country")
    first = food.loc["Chile"]
    print("\n")
    print(first)
    print("\n")


#   4to Módulo: Respuesta a la pregunta n°2
#   > El sig. módulo analiza la columna 'Meat, desde las cuales identifica los
#       valores máximos y mínimos.
def min_max(food):
    os.system("clear")
    food = pd.read_csv("./suministro_alimentos_kcal.csv")
    d = food['Meat'].min()
    u = food['Meat'].max()
    print("\n\tEl mínimo corresponde a: ", d, " y el máximo a: ", u)
    print("\n")


#   5to Módulo: Respuesta a la pregunta n°3
#   > El sig. Módulo hace un conteo general de cada dato asociado a las
#       columnas objetivo ('Confirmed', 'Deaths' y 'Population')
def nro_cmpt(food):
    os.system("clear")
    food = pd.read_csv("./suministro_alimentos_kcal.csv")
    cof = food['Confirmed'].count()
    dead = food['Deaths'].count()
    pop = food['Population'].count()
    print("\n\tHay un nro correspondiente a ", pop,
          " muestras del ítem 'Population', ", cof,
          " a casos 'Confirmed'.")
    print("\n\t Por último, se han contado", dead, " muestras asignables a",
          "categoría 'Deaths'\n\n")


#   6to Módulo: Respuesta a la pregunta n°4
#   > El sig. módulo retorna varias operaciones matemáticas-estadísticas de la
#       columna 'Alcoholic Beverages'
def prom_ol(food):
    os.system("clear")
    food = pd.read_csv("./suministro_alimentos_kcal.csv")
    df = pd.DataFrame(food)
    print("\n")
    print(df['Alcoholic Beverages'].describe())
    print("\n")


#   7to Módulo: Función menú
#   > La función menú, es la función a la cual están asociadas todos los
#       módulos anteriores, y el presionar una tecla específica determina la
#       operación que se llevará a cabo
def menu(food):
    # Limpieza estética de la pantalla (Terminal)
    os.system("clear")
    # Se llama a ciclo_menu, es el flag
    global ciclo_menu
    while ciclo_menu == 0:
        # Información importante de como funciona el programa y sus
        # instrucciones
        print("\n\t      ANÁLISIS DE DATOS A PARTIR DE UN DATASET \n")
        print("""
        El siguiente programa está diseñado y pensado para extraer respuestas a
        partir del análisis de datos de un archivo con extensión .csv
        En el menú que se presentará a continuación, usted podrá pulsar el
        dígito que considere conveniente y extraer sus conclusiones.
        """)

        print("""
        \t1. Mostrar datos de un solo país (Chile)

        \t2. ¿Cuál es el mínimo y el máximo (en Kcal) consumidos de carne entre
            los países enlistados?

        \t3. Contar nro. de muestras de contagios 'Confirmed', muertes 'Deaths'
            y población total 'Population'.

        \t4. Aplicar diversas funciones estadísticas para evaluar el consumo
            total (medido en Kcal) de bebidas alcohólicas.
            """)

        # Se solicita el ingreso del dato por parte del usuario, para realizar
        # la consiguiente operación
        nrop = input("\n\n    >\t")
        # De manera preventiva se insertan varias excepciones, con la finalidad
        # de prevenir y manejar las posibles eventualidades que pudieran
        # ocurrir en el proceso
        try:
            # Acción que realiza la computadora si el usuario pulsa 1:
            if nrop == "1":
                ciclo_menu = 1
                os.system("clear")
                conf_cl('food')

            # Acción que realiza la computadora si el usuario pulsa 2:
            elif nrop == "2":
                ciclo_menu = 1
                os.system("clear")
                min_max('food')

            # Acción que realiza la computadora si el usuario pulsa 3:
            elif nrop == "3":
                ciclo_menu = 1
                os.system("clear")
                nro_cmpt('food')
                graf_tres('food')

            # Acción que realiza la computadora si el usuario pulsa 4:
            elif nrop == "4":
                ciclo_menu = 1
                os.system("clear")
                prom_ol('food')
                graf_cuatro('food')

            # Acción que realiza la computadora si el usuario pulsa cualquier
            # otro número al cual NO se le ha asociado una operación específica
            else:
                os.system("clear")
                print("""
                    \n\n\tLA OPCIÓN INGRESADA NO ES VÁLIDA
            \t INTENTE DE NUEVO!\n\n""")

        # Excepción que maneja el caso donde los operandos son del tipo
        # correcto, pero la operación no tiene sentido para ese valor
        except ValueError:
            os.system("clear")
            print("""
        \n\tEl valor introducido NO es válido, favor intente nuevamente
            e ingrese números válidos.
            """)

        # Excepción que maneja el caso que ocurre al aplicar una operación
        # sobre operandos de tipo incorrecto.
        except TypeError:
            os.system("clear")
            print("""
            \n\tEl tipo ingresado NO es valido, favor intente nuevamente
            e ingrese caracteres válidos.
            """)

        except NameError:
            os.system("clear")
            print("""
            \n\tEl nombre ingresado NO es valido/definido, favor intente
            nuevamente e ingrese caracteres válidos.
            """)

        except (IOError, OSError):
            os.system("clear")
            print("""
        \n\tEl argumento ingresado NO es valido, favor intente nuevamente
            e ingrese caracteres válidos.
            """)

# IMPORTANTE: para que plot funcione deben tener instalada la librería
# "matplotlib", que es la utilizada por defecto
#   8vo Módulo: Gráfico pregunta n° 1

#   9no Módulo: Gráfico pregunta n° 2


#   10mo Módulo: Gráfico pregunta n° 3
def graf_tres(food):
    food = pd.read_csv("./suministro_alimentos_kcal.csv")

    food = food.plot("Country", "Confirmed", kind="bar")
    food.figure.savefig("Confirmed.png")

    food = food.plot("Country", "Deaths", kind="bar")
    food.figure.savefig("Deaths.png")

    food = food.plot("Country", "Population", kind="bar")
    food.figure.savefig("Population.png")


#   11avo Módulo: Gráfico pregunta n° 4
def graf_cuatro(food):
    food = pd.read_csv("./suministro_alimentos_kcal.csv")
    # food.groupby('Country').mean()["Alcoholic Beverages"].plot(kind='bar')
    # food.figure.savefig("alcohol.png")
    # plt.show()
    food = food.plot("Country", "Alcoholic Beverages", kind="bar")
    plt.figure.savefig("Alcohol.png")


#   12avo Módulo: Función principal
def main():
    # Ingresa función de apertura del archivo
    op_file()
    # Inserta menú, que aúna a su vez el resto de las funciones, cada una de
    # las cuales está asociada a una tecla específica
    menu('food')
    # Ingresa funciones de gráficos


# Llamar a función principal si es que es el módulo principal
if __name__ == "__main__":
    main()
